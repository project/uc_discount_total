<?php
/**
 * @file
 * Discounts based on sum of all user's successfully completed purchases. Discount grows depending on orders total amount. */
function uc_discount_total_user($op, &$edit, &$account, $category = NULL) {
  if ($op == 'load') {
  	//TODO: Remove statuses hardcoding
    $total_sum = db_result(db_query("SELECT COALESCE(SUM(price * qty), 0) AS total FROM {uc_order_products} ucpr LEFT JOIN {uc_orders} uc ON ucpr.order_id = uc.order_id  LEFT JOIN {uc_order_statuses} ucs ON uc.order_status = ucs.order_status_id WHERE uid = %d AND state NOT IN('%s', '%s')", $account->uid, 'canceled', 'in_checkout'));
    $account->discount_total = uc_discount_total_personal_rate($total_sum);
  }
}

function uc_discount_total_nodeapi(&$node, $op) {
  global $user;
  switch ($op) {
    case 'load':
      if (uc_product_class_load($node->type)) {
        $node->new_price = uc_discount_total_calc($node->sell_price);
      }
    break;
  }
}

function uc_discount_total_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/store/settings/discount-total', 
      'title' => t('Total Discount Settings'),
      'callback' => 'uc_discount_total_page',
      'access' => user_access('administer uc discount total'),
    );
    $items[]=array(
      'path' => 'admin/store/settings/discount-total/range',
      'title' => t('Order Total Discount'),
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
    );
    $items[]=array(
      'path' => 'admin/store/settings/discount-total/delete',
      'title' => t('Delete discount'),
      'callback' => 'uc_discount_total_delete',
      'callback arguments' => arg(5),
      'type' => MENU_CALLBACK,
      'access' => user_access('administer uc discount total'),
    );
    $items[]=array(
      'path' => 'admin/store/settings/discount-total/edit',
      'title' => t('Edit discount'),
      'callback' => 'uc_discount_total_edit',
      'callback arguments' => arg(5),
      'type' => MENU_CALLBACK,
      'access' => user_access('administer uc discount total'),
    );
    $items[]=array(
      'path' => 'admin/store/settings/discount-total/guest-settings',
      'title' => t('Guest Settings'),
      'callback' => 'drupal_get_form',
      'callback arguments' => 'uc_discount_settings_form',
      'type' => MENU_LOCAL_TASK,
      'access' => user_access('administer uc discount total'),
    );
  }
  return $items;
}

function uc_discount_total_perm() {
  return array('administer uc discount total');
}

function uc_discount_total_page() {
  $output = uc_discount_total_table($collapsed);
  $output .= drupal_get_form('uc_discount_total_add_discount_form');
  return $output;
}

function uc_discount_total_table() {
  $header[] = t('Min summ');
  $header[] = t('Max summ');
  $header[] = t('Rate (%)');
  $header[] = t('Operation');
  $result = db_query("SELECT * FROM {uc_discount_total} ORDER BY total_rate");
  while ($data = db_fetch_object($result)) {
    $row[] = array(
      array(
        'data' => uc_currency_format($data->total_min)),
      array(
        'data' => uc_currency_format($data->total_max)),
      array(
        'data' => $data->total_rate .'%'),
      array(
        'data' => l(t('delete'), 'admin/store/settings/discount-total/delete/'. $data->id) .' | '. 
        l(t('edit'), 'admin/store/settings/discount-total/edit/'. $data->id))
    );
  }
  $output = theme('table', $header, $row);
  return $output;
}

function uc_discount_total_add_discount_form($collapsed = TRUE, $discount = NULL) {
  if ($discount['id']) {
    $title = t('Edit Rate');
    $id = $discount['id'];
    $submit_val = t('Edit Rate');}
  else {
    $title = t('Add Rate');
    $submit_val = t('Add Rate');
  }
  $form['add_discount'] = array(
    '#type' => 'fieldset',
    '#title' => $title,
    '#prefix' => '<div class="discount-form">',
    '#suffix' => '</div>',
    '#collapsible' => TRUE,
    '#collapsed' => $collapsed,
  );
  $form['add_discount']['id'] = array(
    '#type' => 'hidden',
    '#value' => $id,
  );
  $form['add_discount']['min_summ'] = array(
    '#title' => t('Min summ'),
    '#description' => t('Enter the minimum sum for discount charge'),
    '#type' => 'textfield',
    '#default_value' => $discount['total_min'],
    '#required' => TRUE,
  );
  $form['add_discount']['max_summ'] = array(
    '#title' => t('Max summ'),
    '#description' => t('Enter the maximum sum for discount charge'),
    '#default_value' => $discount['total_max'],
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  $form['add_discount']['rate'] =array(
    '#title' => t('Rate (%)'),
    '#description' => t('To specify value without a symbol of %'),
    '#default_value' => $discount['total_rate'],
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  $form['add_discount']['add'] =array(
    '#value' => $submit_val,
    '#type' => 'submit'
  );
  $form['#validate'] = array(
    'number_validate' => array(),
    'range_validate' => array(),
  );
  return $form;
}

function number_validate($form_id, $form_values) {
  //TODO: Make expression simlper.
  $float_reg = '^\d+([.,]\d+){0,1}$';
  $rate_reg = '^\d+([.,]\d+){0,1}\%{0,1}$';
  if (!preg_match("/$float_reg/", $form_values['min_summ'])) {
    form_set_error('min_summ', t('Only positive decimal values are allowed in Min Summ field!'));
  }
  if (!preg_match("/$float_reg/", $form_values['max_summ'])) {
    form_set_error('max_summ', t('Only positive decimal values are allowed in Max Summ field!'));
  }
  if (!preg_match("/$rate_reg/", $form_values['rate'])) {
    form_set_error('rate', t('Only positive decimal values are allowed in Rate field!'));
  }
  if ($form_values['rate'] >= 100) {
    form_set_error('rate', t('Rate should be less or equal to 100%!'));
  }
}
 
function range_validate($form_id, $form_values) {
  if ($form_values['min_summ'] >= $form_values['max_summ']) {
    form_set_error('min_summ', t('Min Summ must not be greater than Max Summ!'));
  }
  $result = db_query("SELECT total_min, total_max FROM {uc_discount_total} WHERE id <> %d", $form_values['id']);
  while ($value = db_fetch_array($result)) {
    if ($value['total_min'] <= $form_values['min_summ'] && $value['total_max'] >= $form_values['min_summ']) {
      form_set_error('min_summ', t('This range intersects with already existing one. Please enter another Min Summ value.'));
    } 
    if ($value['total_min'] <= $form_values['max_summ'] && $value['total_max'] >= $form_values['max_summ']) {
      form_set_error('max_summ', t('This range intersects with already existing one. Please enter another Max Summ value.'));
    }
  }
}

function uc_discount_total_add_discount_form_submit($form_id, $form_values) {
  $trans = array(',' => '.');
  $min_summ = strtr($form_values['min_summ'], $trans);
  $max_summ = strtr($form_values['max_summ'], $trans);
  $rate = strtr($form_values['rate'], $trans);
  if ($form_values['id']) {
    db_query("UPDATE {uc_discount_total} SET total_min = %f, total_max = %f, total_rate = %f
      WHERE id = %d", $min_summ, $max_summ, $rate, $form_values['id']);
    drupal_set_message('Discount has been successfully updated.');
    drupal_goto('admin/store/settings/discount-total');}
  else {
    db_query("INSERT INTO {uc_discount_total} (total_min, 
      total_max, total_rate) VALUES (%f,%f,%f)", $min_summ, $max_summ, $rate);
    drupal_set_message('Discount has been successfully added.');
  }
}

function uc_discount_total_delete($arg) {
  db_query("DELETE FROM {uc_discount_total} WHERE id = %d", $arg);
  drupal_goto('admin/store/settings/discount-total');
}

function uc_discount_total_edit($arg) {
  $discount = db_fetch_array(db_query("SELECT * FROM {uc_discount_total} WHERE id = %d", $arg));
  $output .= drupal_get_form('uc_discount_total_add_discount_form', FALSE, $discount);
  return $output;
}

function uc_discount_total_personal_rate($summ) {
  $rate = db_result(db_query("SELECT total_rate FROM {uc_discount_total} 
    WHERE total_min <= %f AND total_max >= %f", $summ, $summ));
  return $rate;
}

function uc_discount_total_cart_item($op, &$item) {
  switch ($op) {
    case 'load':
      $item->price = uc_discount_total_calc($item->price);
    break;
  }
}

function uc_discount_total_calc($sell_price) {
  global $user;
  if ($user->uid) {
    return round($sell_price / 100 * (100 - $user->discount_total), 2);}
  else {
    return round($sell_price / 100 * (100 - variable_get('guest_discount', '0')), 2);
  }
}

function uc_discount_total_economy_calc($total) {
  global $user;
  if ($user->uid) {
    return round($total * 100 / (100 - $user->discount_total) - $total, 2);}
  else {
    return round($total * 100 / (100 - variable_get('guest_discount', '0')) - $total, 2);
  }
}

function uc_discount_total_checkout_pane() {
  $panes[] = array(
    'id' => 'discount',
    'callback' => 'uc_discount_total_display',
    'title' => t('Discount information'),
    'weight' => 2,
  );
  return $panes;
}

function uc_discount_total_display($op, &$arg1, $arg2) {
  global $user;
  switch ($op) {
    case 'view':
      break;
      //return array('contents'=> array('iuhjkh' => array('#type'=>'textfield')), 'next-button'=>FALSE);
    case 'process':
      return TRUE;
    case 'review':
      if ($user->uid) {

        $output = '<div class="discount">'. t('Your discount is !rate%.', array('!rate' => $user->discount_total)) .'</div>';
        $output .= '<div class="message">'. t('You are saving !save.', array('!save' => uc_currency_format(uc_discount_total_economy_calc($arg1->line_items[0]['amount'])))) .'</div>';}
      elseif (variable_get('guest_discount', '0') != 0) {
        $output = '<div class="discount">'. t('Since you are not a registered user, your discount is only !rate%.', array('!rate' => variable_get('guest_discount', '0'))) .'</div>';
        $output .= '<div class="message">'. t('You are saving !save.', array('!save' => uc_currency_format(uc_discount_total_economy_calc($arg1->line_items[0]['amount'])))) .'</div>';}
      else {
        $output = '<div class="discount">'. t('Since you are not a registered user you have no discount.') .'</div>';
      }
      $review[] = array('title' => t('Discount'), 'data' => $output);
      return $review;
  }
}

function uc_discount_settings_form() {
  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => 'discount_settings_form',
  );
  $form['guest_discount'] = array(
    '#type' => 'textfield',
    '#title' => t('Guest Rate(%)'),
    '#default_value' => variable_get('guest_discount', '0'),
    '#description' => t('The guest discount Rate (%) is applied for all anonymous users. Set rate to 0 to disable discount.')
  );
  return system_settings_form($form);
}